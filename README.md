# Chapter 64: Pipenv for Virtual Environments Management

## Install pipenv package

_Pipenv_ is better virtual environment packages management that _pip_ with `requirements.txt`.

It must be installed globally with the following command:
```shell
pip install pipenv
```

## Create virtual environment

To create virtual environment with _Pipenv_, execute following command:
```shell
pipenv shell
```

This command will create new virtual environment and activate it.
It will also create `Pipfile` file in project directory, wich is holding information about virtual environment and 
required dependencies.

To check which environment is activated, execute following command:
```shell
pipenv --venv
```
It will point to the environment created for this folder.

```shell
pipenv --where
```
This command will point to the working directory of the activated virtual environment.

```shell
which python
```
Will point to the virtual environment's Python executable.

## Install new Python package for virtual environment

To install Python package for activated virtual environment, execute following command:
```shell
pipenv install requests pandas
```
This command will install Python packages, will update `Pipfile` with project dependencies and create `Pipfile.lock`.
`Pipfile.lock` file holds information about packages' versions installed.

To check installed Python packages, execute following command:
```shell
pipenv graph
```
It will show Python packages dependency tree (or graph).

## Update project packages

To update specific project package, execute following command:
```shell
pipenv update pandas
```
This command will update `pandas` package to the latest version and update `Pipfile`.

To update all project dependencies, just execute following command:
```shell
pipenv update
```

## Recreate Virtual Environment using pipenv

When project dependencies need to be install on the other fresh computer, execute following command:
```shell
pipenv install
```
This command will install all required dependencies and update `Pipfile.lock` file. 
It will also create new virtual environment into default virtual environment location.
If there is `.venv` subdirectory in the project directory, then pipenv will create new virtual environment in this 
subdirectory. So if you want to have different location for virtual environment, create this directory before.

If developer don't want to update `Pipfile.lock` file, following command can be used:
```shell
pipenv sync
```
This command will install dependencies from `Pipfile.lock` file without changing it.

To activate virtual environment, execute following command:
```shell
pipenv shell
```
